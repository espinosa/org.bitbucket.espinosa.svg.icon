package org.bitbucket.espinosa.svg.icon.cache;

import java.awt.image.BufferedImage;
import java.util.concurrent.ConcurrentHashMap;

import org.bitbucket.espinosa.svg.icon.rasterizer.BufferedImageFactory;
import org.bitbucket.espinosa.svg.icon.rasterizer.RasterizeSvgException;
import org.bitbucket.espinosa.svg.icon.rasterizer.SvgRasterizer;
import org.bitbucket.espinosa.svg.icon.rasterizer.SvgSource;
import org.bitbucket.espinosa.svg.icon.resmap.ImageResourceMapper;
import org.bitbucket.espinosa.svg.icon.resmap.ImageSourceException;

/**
 * SVG image cached theme repository.<br>
 * Thread safe, backed by {@link ConcurrentHashMap}.
 * 
 * @author Espinosa
 * 
 * @param <T>
 *            final type; for SWT it is {@code org.eclipse.swt.graphics.Image}.
 */
public class CachedImageLoader <T> {
	private final ConcurrentHashMap<String, T> cache = new ConcurrentHashMap<String, T>();

	private final ImageResourceMapper imageSource;
	private final SvgRasterizer rasterizer;
	private final ErrorHandler<T> errorHandler;
	private final BitmapConverter<T> bitmapConversion;

	public CachedImageLoader(
			ImageResourceMapper imageSource, 
			SvgRasterizer rasterizer, 
			BitmapConverter<T> bitmapConversion,
			ErrorHandler<T> errorHandler) {
		this.imageSource = imageSource;
		this.rasterizer = rasterizer;
		this.bitmapConversion = bitmapConversion;
		this.errorHandler = errorHandler;
		rasterizer.setBufferedImageFactory(bitmapConversion.getBufferedImageFactory());
	}

	public T getImage(String imageName) {
		return cache.computeIfAbsent(imageName, this::getImageInternal);
	}

	private T getImageInternal(String imageName) {
		SvgSource svgImage = null;
		BufferedImage bufferedImage = null;
		try {
			svgImage = imageSource.find(imageName);
			bufferedImage = rasterizer.rasterize(svgImage);
			return bitmapConversion.convert(bufferedImage);
		} catch (ImageSourceException | RasterizeSvgException e) {
			return errorHandler.handle(e, imageName, svgImage);
		}	
	}

	/**
	 * Encapsulating error handler for finding, rendering and transforming
	 * image. In case of exception it can propagate it up, wrapped to
	 * {@link CachedImageLoaderException}, or return placeholder image.
	 */
	public interface ErrorHandler<T> {

		/**
		 * @param e cause exception
		 * @param imageName image name
		 * @param svgImage optional, it's null if find() fails.
		 * @return placeholder image or throw wrapping exception
		 */
		T handle(Exception e, String imageName, SvgSource svgImage);
	}

	/**
	 * Default error handler, wrap exception to
	 * {@link CachedImageLoaderException} and propagate it up.
	 */
	public static class PropagatingErrorHandler<T> implements ErrorHandler<T> {
		@Override
		public T handle(Exception e, String imageName, SvgSource svgImage) {
			throw new ProcessingException("Image loading failed on " + imageName, e);
		}
	}

	/**
	 * General wrapping exception. It has to be unchecked because it is used
	 * inside functional interface of the cache.
	 */
	public static class ProcessingException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public ProcessingException(String message, Throwable cause) {
			super(message, cause);
		}
	}

	/**
	 * Convert rendered {@link BufferedImage} to the final form which may be a
	 * different bitmap type; for SWT it is {@code org.eclipse.swt.graphics.Image}.
	 *
	 * @param <T>
	 *            final type; for SWT it is {@code org.eclipse.swt.graphics.Image}.
	 */
	public interface BitmapConverter<T> {
		T convert(BufferedImage bufferedImage);
		BufferedImageFactory getBufferedImageFactory();
	}
}
