package org.bitbucket.espinosa.svg.icon.test.icons2;

import org.bitbucket.espinosa.svg.icon.test.icons.SampleTheme;

/**
 * Few sample icons from Papirus SVG icon theme originally for Gnome/KDE.
 * They are all from 16x16 mimetype and places icons.
 * Unmodified, as it is version.
 * 
 * @see https://github.com/PapirusDevelopmentTeam/papirus-icon-theme
 */
public class SampleTheme2 implements SampleTheme {
	public static SampleTheme INSTANCE = new SampleTheme2();
	public static String[] files = new String[] {
			"folder-yellow.svg", // https://github.com/PapirusDevelopmentTeam/papirus-icon-theme/blob/master/Papirus/16x16/places/folder-yellow.svg
			"folder-blue.svg", // https://github.com/PapirusDevelopmentTeam/papirus-icon-theme/blob/master/Papirus/16x16/places/folder-blue.svg
			"application-x-addon.svg", // https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/16x16/mimetypes/application-x-addon.svg
			"application-x-executable.svg", // https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/16x16/mimetypes/application-x-executable.svg
			"application-x-mswinurl.svg", // https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/16x16/mimetypes/application-x-mswinurl.svg
			"database.svg", // https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/16x16/mimetypes/application-vnd.oasis.opendocument.database.svg
			"unknown.svg",  // https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/16x16/mimetypes/unknown.svg
			"video-x-generic.svg",  // https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/16x16/mimetypes/video-x-generic.svg
			"x-office-document.svg", // https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/16x16/mimetypes/x-office-document.svg
			"x-office-calendar.svg" // https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/Papirus/16x16/mimetypes/x-office-calendar.svg
	};

	@Override
	public String[] getFiles() {
		return files;
	}
	
	@Override
	public String getName() {
		return "Papirus SVG theme 2";
	}
}
