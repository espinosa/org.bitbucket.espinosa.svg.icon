package org.bitbucket.espinosa.svg.icon.test.icons5;

import org.bitbucket.espinosa.svg.icon.test.icons.SampleTheme;

/**
 * Eclipse SVG icons.<br>
 * Source:
 * {@code git://git.eclipse.org/gitroot/platform/eclipse.platform.images.git}
 * respective
 * https://git.eclipse.org/c/platform/eclipse.platform.images.git/plain/org.eclipse.images/eclipse-svg
 * <p>
 * This repository is unfortunately currently (24.7.2017) not mirrored by
 * GitHub; meaning, it cannot be comfortably web browsed. There is some web
 * browsing interface to it too, see link above, but it is not very user
 * friendly and has bugs. Most painful bug is that SVG files cannot be
 * downloaded through it. Its <i>plain</i> file view, aka <i>raw</i> view,
 * normally used to download individual files, this functionality has a bug,
 * where SVG output, when viewed or downloaded via browser, is damaged; all
 * colors and styles are lost and image appears as a black shape only. Huh.
 * 
 * I strongly recommend to clone
 * {@code git://git.eclipse.org/gitroot/platform/eclipse.platform.images.git}
 * repository.
 */
public class SampleTheme5 implements SampleTheme {
	public static SampleTheme INSTANCE = new SampleTheme5();
	public static String[] files = new String[] {
			"folder.svg",            // /org.eclipse.ui.ide/icons/full/obj16/folder.svg
			"activity_category.svg", // /org.eclipse.ui/icons/full/obj16/activity_category.svg
			"fldr_obj.svg",          // /org.eclipse.ui/icons/full/obj16/fldr_obj.svg (multiple locations)
			"file_obj.svg",          // /org.eclipse.ui/icons/full/obj16/file_obj.svg (multiple locations)
			"stop.svg",              // /org.eclipse.ui/icons/full/elcl16/stop.svg
			"synced.svg",            // /org.eclipse.ui/icons/full/elcl16/synced.svg
			"up_nav.svg",            // /org.eclipse.ui/icons/full/elcl16/up_nav.svg
			"forward_nav.svg",       // /org.eclipse.ui/icons/full/elcl16/forward_nav.svg
			"backward_nav.svg",      // /org.eclipse.ui/icons/full/elcl16/backward_nav.svg
			"linkto_help.svg",       // /org.eclipse.ui/icons/full/elcl16/linkto_help.svg
			"at_symbol.svg",         // /org.eclipse.jdt.ui/icons/full/eview16/javadoc.svg
			"plain_file.svg",        // /org.eclipse.ui.ide/icons/full/obj16/fileType_filter.svg
			"folder_and_file.svg",   // /org.eclipse.ui.ide/icons/full/obj16/fileFolderType_filter.svg
			"jar_obj.svg"            // /org.eclipse.jdt.debug.ui/icons/full/obj16/jar_obj.svg
	};

	// Note1:
	// All paths above are relative to: <project root>/org.eclipse.images/eclipse-svg
	//
	// Note2:
	// relative path: <project root>/org.eclipse.images/eclipse-svg/org.eclipse.ui/icons/full/elcl16/stop.svg
	// can be found through web interface as:
	// https://git.eclipse.org/c/platform/eclipse.platform.images.git/tree/org.eclipse.images/eclipse-svg/org.eclipse.ui/icons/full/elcl16/stop.svg
		
	@Override
	public String[] getFiles() {
		return files;
	}
	
	@Override
	public String getName() {
		return "Eclipse icons";
	}
}
