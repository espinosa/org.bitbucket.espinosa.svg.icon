package org.bitbucket.espinosa.svg.icon.test.icons3;

import org.bitbucket.espinosa.svg.icon.test.icons.SampleTheme;

/**
 * Directory/Folder icons from various places in the Internet.
 * This icons have various "native" sizes and are mostly not optimised for small sizes.
 * Some SVG had to be manually fixed to work with Batik.
 * 
 * TODO: remove and replace with SampleTheme4 once auto-fixer is in place (SampleTheme4 same icons, just ensured that no fixes were applied)
 */
public class SampleTheme3 implements SampleTheme {
	public static SampleTheme INSTANCE = new SampleTheme3();
	public static String[] files = new String[] {
			"Antu_folder-yellow.svg", // https://upload.wikimedia.org/wikipedia/commons/8/8d/Antu_folder-yellow.svg
			"Breezeicons-places-64-folder-orange-fix.svg", // https://upload.wikimedia.org/wikipedia/commons/d/d7/Breezeicons-places-64-folder-orange.svg
			"eclipse-activity_category.svg",
			"eclipse-fldr_obj.svg",
			"eclipse-folder.svg",
			"Folder-blue-2.0.svg", // https://upload.wikimedia.org/wikipedia/commons/6/6f/Folder-blue-2.0.svg
			"Folder-blue.svg",     // https://upload.wikimedia.org/wikipedia/commons/f/fb/Folder-blue.svg
			"Inscape_edited_3d_blue_folder.svg",
			   // modified version of https://upload.wikimedia.org/wikipedia/commons/f/fb/Folder-blue.svg 
			   // modified by me, mostly to enlarge it
			"Folder-open_Gion.svg", // https://upload.wikimedia.org/wikipedia/commons/8/84/Folder-saved-search.svg
			"Folder-saved-search.svg", // https://upload.wikimedia.org/wikipedia/commons/4/4b/Gnome-folder-saved-search.svg
			"Gnome-folder-saved-search.svg", // https://upload.wikimedia.org/wikipedia/commons/4/4b/Gnome-folder-saved-search.svg
			"Icons8_flat_opened_folder.svg" // https://upload.wikimedia.org/wikipedia/commons/b/bf/Icons8_flat_opened_folder.svg

	};

	@Override
	public String[] getFiles() {
		return files;
	}
	
	@Override
	public String getName() {
		return "Mixed source folder icons theme (edited)";
	}
}
