package org.bitbucket.espinosa.svg.icon.test.ui;

import org.bitbucket.espinosa.svg.icon.cache.CachedImageLoader;
import org.bitbucket.espinosa.svg.icon.cache.CachedImageLoader.PropagatingErrorHandler;
import org.bitbucket.espinosa.svg.icon.rasterizer.BatikRasterizer;
import org.bitbucket.espinosa.svg.icon.rasterizer.FixedOutputSizeDefinition;
import org.bitbucket.espinosa.svg.icon.rasterizer.HighQualityHintConfiguration;
import org.bitbucket.espinosa.svg.icon.rasterizer.fixers.DefaultSourceFixers;
import org.bitbucket.espinosa.svg.icon.resmap.SimpleResourceIcons;
import org.bitbucket.espinosa.svg.icon.swt.BufferedImageToSwtImageConverter1;
import org.bitbucket.espinosa.svg.icon.test.icons.SampleTheme;
import org.bitbucket.espinosa.svg.icon.test.icons4.SampleTheme4;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

/**
 * Test rendering by showing few hand picked icons in a simple (SWT Shell) GUI window.
 * Use SWT list to display icons alongside their filename names.
 * 
 * There is more advanced example using in
 * IconShowcase (/org.bitbucket.espinosa.e4rcp.playground/src/org/bitbucket/espinosa/icshow/IconShowcase.java)
 * to see full directory listing using NatTable and listing all icons in the icons directory.
 * 
 * @author Espinosa
 */
public class ShowSelectedIcons {
	private SampleTheme theme = new SampleTheme4();

	private TableViewer viewer;

	private final CachedImageLoader<Image> iml = new CachedImageLoader<>(
			new SimpleResourceIcons(theme.getClass()),
			new BatikRasterizer(
					new FixedOutputSizeDefinition(16, 16),
					new HighQualityHintConfiguration(),
					new DefaultSourceFixers()),
			new BufferedImageToSwtImageConverter1(),
			new PropagatingErrorHandler<Image>()
			);

	public void createControls(Composite parent) {
		Composite tableComposite = new Composite(parent, SWT.FILL);
		TableColumnLayout tableColumnLayout = new TableColumnLayout();
		tableComposite.setLayout(tableColumnLayout);
		// TableColumnLayout can be set ONLY to a container whose ONLY child is the Table. 
		// Don't assign the layout directly the Table 

		viewer = new TableViewer(tableComposite, 
				SWT.FILL | SWT.SINGLE | SWT.FULL_SELECTION
				);
		Table table = viewer.getTable();
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(false);

		TableViewerColumn column1 = new TableViewerColumn(viewer, SWT.NONE);
		column1.setLabelProvider(new IconLabelProvider());
		column1.getColumn().setText("Icon");
		column1.getColumn().setAlignment(SWT.LEFT);
		tableColumnLayout.setColumnData(column1.getColumn(), new ColumnPixelData(60)); // fixed size column

		TableViewerColumn column2 = new TableViewerColumn(viewer, SWT.NONE);
		column2.setLabelProvider(new NameLabelProvider());
		column2.getColumn().setText("Name");
		column2.getColumn().setAlignment(SWT.LEFT);
		tableColumnLayout.setColumnData(column2.getColumn(), new ColumnWeightData(100)); // fill 100% of the remaining horizontal space

		viewer.setContentProvider(new IconListContentProvider());
	}
	
	public void loadIcons() {
		viewer.setInput(org.bitbucket.espinosa.svg.icon.test.icons2.SampleTheme2.files);
	}

	class IconLabelProvider extends StyledCellLabelProvider {
		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			String svgFileName = (String) element;
			Image swtImage = iml.getImage(svgFileName);
			cell.setImage(swtImage);			
			super.update(cell);
		}
	}

	class NameLabelProvider extends StyledCellLabelProvider {
		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			String svgFileName = (String) element;
			cell.setText(svgFileName);
			super.update(cell);
		}
	}

	public class IconListContentProvider implements IStructuredContentProvider {
		@Override
		public Object[] getElements(Object inputElement) {
			return theme.getFiles();
		}
	}

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(new Point(500, 400));
		shell.setLayout(new FillLayout());
		ShowSelectedIcons app = new ShowSelectedIcons();
		app.createControls(shell);
		shell.open();
		
		app.loadIcons();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
