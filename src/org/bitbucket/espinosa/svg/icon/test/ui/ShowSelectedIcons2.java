package org.bitbucket.espinosa.svg.icon.test.ui;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.bitbucket.espinosa.svg.icon.cache.CachedImageLoader;
import org.bitbucket.espinosa.svg.icon.cache.CachedImageLoader.PropagatingErrorHandler;
import org.bitbucket.espinosa.svg.icon.rasterizer.BatikRasterizer;
import org.bitbucket.espinosa.svg.icon.rasterizer.FixedOutputSizeDefinition;
import org.bitbucket.espinosa.svg.icon.rasterizer.HighQualityHintConfiguration;
import org.bitbucket.espinosa.svg.icon.rasterizer.fixers.DefaultSourceFixers;
import org.bitbucket.espinosa.svg.icon.resmap.SimpleResourceIcons;
import org.bitbucket.espinosa.svg.icon.swt.BufferedImageToSwtImageConverter1;
import org.bitbucket.espinosa.svg.icon.test.icons.SampleTheme;
import org.bitbucket.espinosa.svg.icon.test.icons1.SampleTheme1;
import org.bitbucket.espinosa.svg.icon.test.icons2.SampleTheme2;
import org.bitbucket.espinosa.svg.icon.test.icons3.SampleTheme3;
import org.bitbucket.espinosa.svg.icon.test.icons4.SampleTheme4;
import org.bitbucket.espinosa.svg.icon.test.icons5.SampleTheme5;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.OwnerDrawLabelProvider;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

/**
 * Test rendering by showing few hand picked icons in a simple (SWT Shell) GUI window.
 * Use SWT list to display icons alongside their filename names.
 * 
 * There is more advanced example using in
 * IconShowcase (/org.bitbucket.espinosa.e4rcp.playground/src/org/bitbucket/espinosa/icshow/IconShowcase.java)
 * to see full directory listing using NatTable and listing all icons in the icons directory.
 * 
 * @author Espinosa
 */
public class ShowSelectedIcons2 {
	private SampleTheme theme = SampleTheme5.INSTANCE;

	private TableViewer viewer;
	
	private Shell shell;

	private CachedImageLoader<Image> iml;

	private int iconSize = 16;
	
	private Label status;
	
	private void createCache() {
		iml = new CachedImageLoader<>(
				new SimpleResourceIcons(theme.getClass()),
				new BatikRasterizer(
						new FixedOutputSizeDefinition(iconSize, iconSize),
						new HighQualityHintConfiguration(),
						new DefaultSourceFixers()),
				new BufferedImageToSwtImageConverter1(),
				new PropagatingErrorHandler<Image>()
				);
	}

	public void createControls(Shell shell) {
		this.shell = shell;
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 0;
	    gridLayout.marginWidth = 0;
	    shell.setLayout(gridLayout);
	    
		createTable(shell);
		createStatusLabel(shell);
		createMenus(shell); 
	}
	
	void createStatusLabel(Composite parent) {
		status = new Label(parent, SWT.NONE);
		status.setText("starting..");
		GridDataFactory.fillDefaults().grab(true, false).applyTo(status);
	}
	
	void createTable(Composite parent) {
		Composite tableComposite = new Composite(parent, SWT.FILL);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(tableComposite);
		
		TableColumnLayout tableColumnLayout = new TableColumnLayout();
		tableComposite.setLayout(tableColumnLayout);
		// TableColumnLayout can be set ONLY to a container whose ONLY child is the Table. 
		// Don't assign the layout directly the Table 

		viewer = new TableViewer(tableComposite, 
				SWT.FILL | SWT.SINGLE | SWT.FULL_SELECTION  //| SWT.BORDER
				);
		Table table = viewer.getTable();
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(false);
		table.setLinesVisible(false);

		TableViewerColumn column1 = new TableViewerColumn(viewer, SWT.NONE);
		column1.setLabelProvider(new IconLabelProvider());
		column1.getColumn().setText("Icon");
		column1.getColumn().setAlignment(SWT.LEFT);
		tableColumnLayout.setColumnData(column1.getColumn(), new ColumnPixelData(iconSize)); // fixed size column

		TableViewerColumn column2 = new TableViewerColumn(viewer, SWT.NONE);
		column2.setLabelProvider(new NameLabelProvider());
		column2.getColumn().setText("Name");
		column2.getColumn().setAlignment(SWT.LEFT);
		tableColumnLayout.setColumnData(column2.getColumn(), new ColumnWeightData(100)); // fill 100% of the remaining horizontal space

		viewer.setContentProvider(new IconListContentProvider());
	}

	void createMenus(Shell shell) {
		Menu menuBar = new Menu(shell, SWT.BAR);

		MenuItem menuItem1 = new MenuItem(menuBar, SWT.CASCADE);
		menuItem1.setText("&Theme");

		MenuItem menuItem2 = new MenuItem(menuBar, SWT.CASCADE);
		menuItem2.setText("&Size");

		{
			Menu menu1 = new Menu(shell, SWT.DROP_DOWN);;
			menuItem1.setMenu(menu1);

			MenuItem menuItem11 = new MenuItem(menu1, SWT.PUSH);
			menuItem11.setText("theme 1");
			menuItem11.addListener(SWT.Selection, event-> {
				changeTheme(SampleTheme1.INSTANCE);
			});

			MenuItem menuItem12 = new MenuItem(menu1, SWT.PUSH);
			menuItem12.setText("theme 2");
			menuItem12.addListener(SWT.Selection, event-> {
				changeTheme(SampleTheme2.INSTANCE);
			});

			MenuItem menuItem13 = new MenuItem(menu1, SWT.PUSH);
			menuItem13.setText("theme 3");
			menuItem13.addListener(SWT.Selection, event-> {
				changeTheme(SampleTheme3.INSTANCE);
			});

			MenuItem menuItem14 = new MenuItem(menu1, SWT.PUSH);
			menuItem14.setText("theme 4");
			menuItem14.addListener(SWT.Selection, event-> {
				changeTheme(SampleTheme4.INSTANCE);
			});

			MenuItem menuItem15 = new MenuItem(menu1, SWT.PUSH);
			menuItem15.setText("theme 5");
			menuItem15.addListener(SWT.Selection, event-> {
				changeTheme(SampleTheme5.INSTANCE);
			});
		}

		{
			Menu menu2 = new Menu(shell, SWT.DROP_DOWN);;
			menuItem2.setMenu(menu2);

			MenuItem menuItem21 = new MenuItem(menu2, SWT.PUSH);
			menuItem21.setText("16x16");
			menuItem21.addListener(SWT.Selection, event-> {
				changeSize(16);
			});

			MenuItem menuItem22 = new MenuItem(menu2, SWT.PUSH);
			menuItem22.setText("32x32");
			menuItem22.addListener(SWT.Selection, event-> {
				changeSize(32);
			});

			MenuItem menuItem23 = new MenuItem(menu2, SWT.PUSH);
			menuItem23.setText("48x48");
			menuItem23.addListener(SWT.Selection, event-> {
				changeSize(48);
			});

			MenuItem menuItem24 = new MenuItem(menu2, SWT.PUSH);
			menuItem24.setText("64x64");
			menuItem24.addListener(SWT.Selection, event-> {
				changeSize(64);
			});
		}

		shell.setMenuBar(menuBar);
	}

	public void setupCache() {
		createCache();
		reloadIcons();
	}

	public void resetCache() {
		createCache();
		reloadIcons();
	}

	public void loadIcons() {
		viewer.setInput(theme);
	}

	public void reloadIcons() {
		viewer.getTable().getColumn(0).setWidth(iconSize + 6);
		viewer.setInput(theme);
	}

	class IconLabelProvider extends OwnerDrawLabelProvider {
		@Override
		protected void measure(Event event, Object element) {
			String svgFileName = (String) element;
			Image swtImage = iml.getImage(svgFileName);
			Rectangle rectangle = swtImage.getBounds();
			event.setBounds(new Rectangle(
					event.x, event.y,
					rectangle.width, rectangle.height));
		}

		@Override
		protected void paint(Event event, Object element) {        	
			String svgFileName = (String) element;
			Image swtImage = iml.getImage(svgFileName);
			Rectangle bounds = event.getBounds();
			event.gc.drawImage(swtImage, bounds.x, bounds.y);
		}
	};

	class NameLabelProvider extends StyledCellLabelProvider {
		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			String svgFileName = (String) element;
			cell.setText(svgFileName);
			super.update(cell);
		}
	}

	public class IconListContentProvider implements IStructuredContentProvider {
		@Override
		public Object[] getElements(Object sampleTheme) {
			return ((SampleTheme)sampleTheme).getFiles();
		}
	}
	
	public void afterOpen() {
		setupCache();
		loadIcons();
		status.setText(theme.getName() + " size " + iconSize + "x" + iconSize);
	}
	
	public void changeSize(int newSize) {
		setItemHeight(newSize);
		this.iconSize = newSize;
		resetCache();
		reloadIcons();
		status.setText(theme.getName() + " size " + iconSize + "x" + iconSize);
		shell.redraw();
	}

	// https://bugs.eclipse.org/bugs/show_bug.cgi?id=154341
	// There is a bug when tale row has to be made smaller. With getting bigger all works transparently and without issue, just by setting event bounds. 
	// When however row has to be made smaller, it stays same; no way how to shrink it.
	private void setItemHeight(int height) {
		Table table = viewer.getTable();
		try {
			Method m = table.getClass().getDeclaredMethod("setItemHeight", int.class);
			m.setAccessible(true);
			m.invoke(table, height);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	public void changeTheme(SampleTheme newTheme) {
		this.theme = newTheme;
		resetCache();
		reloadIcons();
		status.setText(theme.getName() + " size " + iconSize + "x" + iconSize);
	}

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setText("SVG Icon Theme Showcase");
		shell.setSize(new Point(500, 400));
		ShowSelectedIcons2 app = new ShowSelectedIcons2();
		app.createControls(shell);
		shell.open();

		app.afterOpen();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
