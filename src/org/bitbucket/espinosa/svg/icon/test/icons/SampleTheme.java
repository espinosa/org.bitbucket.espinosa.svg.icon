package org.bitbucket.espinosa.svg.icon.test.icons;

public interface SampleTheme {
	String[] getFiles();
	String getName();
}
