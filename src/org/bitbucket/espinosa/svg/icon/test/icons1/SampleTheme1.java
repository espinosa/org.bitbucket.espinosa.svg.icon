package org.bitbucket.espinosa.svg.icon.test.icons1;

import org.bitbucket.espinosa.svg.icon.test.icons.SampleTheme;

/**
 * Few sample icons from Papirus SVG icon theme originally for Gnome/KDE.
 * They are all from 16x16 mimetype and places icons.
 * Some might (?) have been modified to run with Batik.
 * 
 * TODO: remove and replace with SampleTheme2 (same icons, just ensured that no fixes were applied)
 * 
 * @see https://github.com/PapirusDevelopmentTeam/papirus-icon-theme 
 * @see https://github.com/PapirusDevelopmentTeam/papirus-icon-theme/blob/master/Papirus/16x16/mimetypes/x-office-document.svg
 */
public class SampleTheme1 implements SampleTheme {
	public static SampleTheme INSTANCE = new SampleTheme1();
	String[] files = new String[] {
			"application-x-addon.svg",
			"application-x-executable.svg",
			"application-x-mswinurl.svg",
			"database.svg",
			"unknown.svg",
			"video-x-generic.svg",
			"x-office-document.svg",
			"x-office-calendar.svg"
	};

	@Override
	public String[] getFiles() {
		return files;
	}

	@Override
	public String getName() {
		return "Papirus SVG theme";
	}
}
