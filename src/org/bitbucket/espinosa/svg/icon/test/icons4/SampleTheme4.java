package org.bitbucket.espinosa.svg.icon.test.icons4;

import org.bitbucket.espinosa.svg.icon.test.icons.SampleTheme;

/**
 * Directory/Folder icons from various places in the Internet.
 * This icons have various "native" sizes and are mostly not optimised for small sizes.
 * Original, unedited downloads. Some were trouble makers.
 */
public class SampleTheme4 implements SampleTheme {
	public static SampleTheme INSTANCE = new SampleTheme4();
	public static String[] files = new String[] {
			"Antu_folder-yellow.svg", // https://upload.wikimedia.org/wikipedia/commons/8/8d/Antu_folder-yellow.svg
			  // Issue: The attribute "offset" of the element <stop> is required
			"Breezeicons-places-64-folder-orange.svg", // https://upload.wikimedia.org/wikipedia/commons/d/d7/Breezeicons-places-64-folder-orange.svg
			   // Issue: The attribute "offset" of the element <stop> is required
			
//			"curly_bracket_file.svg",
			// Source: https://git.eclipse.org/c/platform/eclipse.platform.images.git/tree/org.eclipse.images/eclipse-svg//org.eclipse.ui.examples.javaeditor/icons/obj16/java.svg
			// Issue [1]: URL data in unsupported format or corrupt
			// Caused by: org.apache.batik.transcoder.TranscoderException: null

			"Folder-blue-2_0.svg", // https://upload.wikimedia.org/wikipedia/commons/6/6f/Folder-blue-2.0.svg
			"Folder-blue.svg",     // https://upload.wikimedia.org/wikipedia/commons/f/fb/Folder-blue.svg
			"Folder-open_Gion.svg", // https://upload.wikimedia.org/wikipedia/commons/3/38/Folder-open_Gion.svg
			"Folder-saved-search.svg", // https://upload.wikimedia.org/wikipedia/commons/8/84/Folder-saved-search.svg
			"Gnome-folder-saved-search.svg", // https://upload.wikimedia.org/wikipedia/commons/4/4b/Gnome-folder-saved-search.svg
			"Icons8_flat_opened_folder.svg" // https://upload.wikimedia.org/wikipedia/commons/b/bf/Icons8_flat_opened_folder.svg
	};

	@Override
	public String[] getFiles() {
		return files;
	}
	
	@Override
	public String getName() {
		return "Mixed source folder icons theme (unedited)";
	}
}

// [1]
// Caused by: org.apache.batik.transcoder.TranscoderException: null
// Enclosed Exception:
// The URI "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhki....
// on element <image> can't be opened because:
// URL data in unsupported format or corrupt
//	at org.apache.batik.transcoder.SVGAbstractTranscoder.transcode(SVGAbstractTranscoder.java:227)
//	at org.apache.batik.transcoder.image.ImageTranscoder.transcode(ImageTranscoder.java:92)
//	at org.apache.batik.transcoder.XMLAbstractTranscoder.transcode(XMLAbstractTranscoder.java:142)
//	at org.apache.batik.transcoder.SVGAbstractTranscoder.transcode(SVGAbstractTranscoder.java:156)
//	at org.bitbucket.espinosa.svg.icon.rasterizer.BatikRasterizer.renderIconInternal(BatikRasterizer.java:124)
