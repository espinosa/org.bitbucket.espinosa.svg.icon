package org.bitbucket.espinosa.svg.icon.swt;

import java.awt.image.BufferedImage;

import org.eclipse.swt.graphics.ImageData;

/**
 * Convert {@link BufferedImage} to {@link ImageData} format required SWT UI.
 * 
 * @author Espinosa
 */
public interface BufferedImageToSwtImageDataConverter {
	ImageData convertToSWT(BufferedImage bufferedImage);
}
