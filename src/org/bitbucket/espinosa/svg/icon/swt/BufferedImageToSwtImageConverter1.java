package org.bitbucket.espinosa.svg.icon.swt;

import java.awt.image.BufferedImage;

import org.bitbucket.espinosa.svg.icon.cache.CachedImageLoader.BitmapConverter;
import org.bitbucket.espinosa.svg.icon.rasterizer.BufferedImageFactory;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

/**
 * {@link BufferedImage} to SWT {@link Image} Converter using recommended
 * {@link BufferedImageToSwtImageDataConverter1}
 * 
 * @author Espinosa
 */
public class BufferedImageToSwtImageConverter1 implements BitmapConverter<Image> {
	private final Display display = Display.getDefault();
	private final BufferedImageToSwtImageDataConverter convertor = new BufferedImageToSwtImageDataConverter1();
	
	@Override
	public Image convert(BufferedImage bufferedImage) {
		return new Image(display, convertor.convertToSWT(bufferedImage));
	}

	@Override
	public BufferedImageFactory getBufferedImageFactory() {
		return (int w, int h)-> {
			return new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		};
	}
}
