package org.bitbucket.espinosa.svg.icon.resmap;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.bitbucket.espinosa.svg.icon.rasterizer.SvgSource;

/**
 * Icons in class-path resources with simple one root directory layout.
 * 
 * @author Espinosa
 */
public class SimpleResourceIcons2 implements ImageResourceMapper {
	private final URI resourceDirectory;

	public SimpleResourceIcons2(String directory) {
		// see: https://stackoverflow.com/questions/7602289/class-getresource-returns-null-in-my-eclipse-application-cant-configure-clas/7602425#7602425
		// see: https://stackoverflow.com/questions/1771679/difference-between-threads-context-class-loader-and-normal-classloader
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		URL resourceDirectoryUrl = loader.getResource(directory);
		if (resourceDirectoryUrl == null) {
			throw new IllegalArgumentException("Resource " + directory + " not found");
		}
		try {
			this.resourceDirectory = resourceDirectoryUrl.toURI();
		} catch (URISyntaxException e) {
			throw new IllegalArgumentException("Directory URL cannot be transformed to URI: " + resourceDirectoryUrl);
		}

	}

	@Override
	public SvgSource find(String name) throws ImageSourceException {
		if (name == null) {
			throw new IllegalArgumentException("Icon name cannot be null");
		}

		URI resourceURI = resourceDirectory.resolve(name);

		InputStream stream;
		try {
			stream = resourceDirectory.resolve(name).toURL().openStream();
		} catch (IOException e) {
			throw new ImageSourceException("Failed to open " + name 
					+ "; " + resourceURI, e);
		}

		if (stream == null) {
			throw new ImageSourceException("Failed to open " + name 
					+ "; stream == null; " + resourceURI);
		}

		return new SvgSource(name, stream, resourceURI);
	}
}
