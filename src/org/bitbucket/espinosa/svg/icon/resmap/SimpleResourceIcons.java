package org.bitbucket.espinosa.svg.icon.resmap;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.bitbucket.espinosa.svg.icon.rasterizer.SvgSource;

/**
 * Icons in class-path resources with simple one root directory layout.
 * 
 * @author Espinosa
 */
public class SimpleResourceIcons implements ImageResourceMapper {
	private final Class<?> themeRoot;

	public SimpleResourceIcons(Class<?> themeRoot) {
		this.themeRoot = themeRoot;
	}

	@Override
	public SvgSource find(String name) throws ImageSourceException {
		if (name == null) {
			throw new IllegalArgumentException("Icon name cannot be null");
		}

		URL resourceURL = themeRoot.getResource(name);
		if (resourceURL == null) {
			throw new ImageSourceException("Resource " + name + " not found");
		}

		URI resourceURI;		
		try {
			resourceURI = resourceURL.toURI();
		} catch (URISyntaxException e) {
			throw new ImageSourceException("Resource " + resourceURL + " cannot be convertd to URI");
		}

		InputStream stream = themeRoot.getResourceAsStream(name);

		if (stream == null) {
			throw new ImageSourceException("Failed to open " + name 
					+ "; " + themeRoot.getResource(name).toString());
		}

		return new SvgSource(name, stream, resourceURI);
	}
}
