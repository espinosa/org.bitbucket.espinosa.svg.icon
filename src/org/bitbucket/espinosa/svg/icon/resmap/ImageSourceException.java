package org.bitbucket.espinosa.svg.icon.resmap;

public class ImageSourceException extends Exception {
	private static final long serialVersionUID = 1L;

	public ImageSourceException() {
		super();
	}

	public ImageSourceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ImageSourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ImageSourceException(String message) {
		super(message);
	}

	public ImageSourceException(Throwable cause) {
		super(cause);
	}
}
