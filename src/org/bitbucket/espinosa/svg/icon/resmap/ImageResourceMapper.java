package org.bitbucket.espinosa.svg.icon.resmap;

import org.bitbucket.espinosa.svg.icon.rasterizer.SvgSource;

/**
 * Image Resource Mapper. Give SVG image stream on given name. IRM should deal
 * with locations (class resource, external directory..) and potentially complex
 * layouts (like Papyrus theme and other nice KDE and Gnome themes).
 * 
 * TODO: add probing method
 * 
 * @author Espinosa
 */
public interface ImageResourceMapper {
	
	/**
	 * Give SVG image stream on given name.
	 * 
	 * @param name svg file name
	 * 
	 * @return wrapping entity around input stream and (optional) absolute path descriptor
	 */
	SvgSource find(String name) throws ImageSourceException;
}
