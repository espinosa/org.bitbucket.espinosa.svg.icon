package org.bitbucket.espinosa.svg.icon.rasterizer;

import org.w3c.dom.svg.SVGDocument;

public class FixedOutputSizeDefinition implements OutputSizeDefinition {
	private final OutputSize size;
	
	public FixedOutputSizeDefinition(int width, int height) {
		size = new OutputSize(width, height);
	}
	
	@Override
	public OutputSize extract(SVGDocument svgDocument, SvgSource icon) {
		return size;
	}
}
