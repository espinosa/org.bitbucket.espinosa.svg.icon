package org.bitbucket.espinosa.svg.icon.rasterizer;

import java.awt.image.BufferedImage;

public interface SvgRasterizer {
	BufferedImage rasterize(SvgSource icon) throws RasterizeSvgException;
	void setBufferedImageFactory(BufferedImageFactory bufferedImageFactory);
}
