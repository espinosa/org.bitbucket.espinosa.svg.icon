package org.bitbucket.espinosa.svg.icon.rasterizer;

import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

public class NativeOutputSize implements OutputSizeDefinition {
	private final double outputScale;
	
	public NativeOutputSize() {
		this(1);
	}

	public NativeOutputSize(double outputScale) {
		this.outputScale = outputScale;
	}

	@Override
	public OutputSize extract(SVGDocument svgDocument, SvgSource icon) throws RasterizeSvgException {
		int[] nativeSize = extractNativeSize(svgDocument, icon);
		int nativeWidth = nativeSize[0];
		int nativeHeight = nativeSize[1];
		int outputWidth = (int) (nativeWidth * outputScale);
		int outputHeight = (int) (nativeHeight * outputScale);
		return new OutputSize(outputWidth, outputHeight);
	}
	
	private int[] extractNativeSize(SVGDocument svgDocument, SvgSource icon) throws RasterizeSvgException {
		Element svgDocumentNode = svgDocument.getDocumentElement();
		String nativeWidthStr = svgDocumentNode.getAttribute("width");
		String nativeHeightStr = svgDocumentNode.getAttribute("height");
		int nativeWidth = -1;
		int nativeHeight = -1;

		try {
			if (!"".equals(nativeWidthStr) && !"".equals(nativeHeightStr)) {
				nativeWidthStr = stripOffPx(nativeWidthStr);
				nativeHeightStr = stripOffPx(nativeHeightStr);
				nativeWidth = Integer.parseInt(nativeWidthStr);
				nativeHeight = Integer.parseInt(nativeHeightStr);
			} else {
				// Vector graphics editing programs don't always output height
				// and width attributes on SVG.
				// As fall back: parse the viewBox attribute (which is almost
				// always set).
				String viewBoxStr = svgDocumentNode.getAttribute("viewBox");
				if ("".equals(viewBoxStr)) {
					//					log.error("Icon defines neither width/height nor a viewBox, skipping: " + icon.nameBase);
					//					failedIcons.add(icon);
					//					return;
					throw new RasterizeSvgException("Icon defines neither width/height nor a viewBox", icon);
				}
				String[] splitted = viewBoxStr.split(" ");
				if (splitted.length != 4) {
					//					log.error("Dimension could not be parsed. Skipping: " + icon.nameBase);
					//					failedIcons.add(icon);
					//					return;
					throw new RasterizeSvgException("Dimension could not be parsed", icon);
				}
				String widthStr = splitted[2];
				widthStr = stripOffPx(widthStr);
				String heightStr = splitted[3];
				heightStr = stripOffPx(heightStr);

				nativeWidth = Integer.parseInt(widthStr);
				nativeHeight = Integer.parseInt(heightStr);
			}
		} catch (NumberFormatException e) {
			//			log.error("Dimension could not be parsed ( " + e.getMessage() + "), skipping: " + icon.nameBase);
			//			failedIcons.add(icon);
			//			return;
			throw new RasterizeSvgException("Dimension could not be parsed (" + e.getMessage() + ")", icon);
		}
		return new int[]{nativeWidth, nativeHeight};
	}

	private String stripOffPx(String dimensionString) {
		if (dimensionString.endsWith("px")) {
			return dimensionString.substring(0, dimensionString.length() - 2);
		}
		return dimensionString;
	}
}
