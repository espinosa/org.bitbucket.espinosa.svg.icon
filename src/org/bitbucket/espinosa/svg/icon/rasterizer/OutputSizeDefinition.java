package org.bitbucket.espinosa.svg.icon.rasterizer;

import org.w3c.dom.svg.SVGDocument;

public interface OutputSizeDefinition {
	OutputSize extract(SVGDocument svgDocument, SvgSource icon) throws RasterizeSvgException;
}
