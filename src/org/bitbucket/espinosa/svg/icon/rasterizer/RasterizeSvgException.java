package org.bitbucket.espinosa.svg.icon.rasterizer;

public class RasterizeSvgException extends Exception {
	private static final long serialVersionUID = 1L;
	private SvgSource source;

	public RasterizeSvgException(String message, SvgSource source, Throwable cause) {
		super(message, cause);
		this.source = source;
	}

	public RasterizeSvgException(String message, SvgSource source) {
		super(message);
		this.source = source;
	}
	
	public SvgSource getSource() {
		return source;
	}
	
	@Override
    public String toString() {
        String className = getClass().getName();
        String message = getLocalizedMessage();
        if (message != null && source != null) {
        	return className + ": " + message + ": source=" + source.getResourceURI();
        } else if (message != null) {
        	return className + ": " + message;
        } else {
        	return className;
        }
    }
}
