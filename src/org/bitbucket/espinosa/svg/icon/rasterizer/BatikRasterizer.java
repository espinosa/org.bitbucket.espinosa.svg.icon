package org.bitbucket.espinosa.svg.icon.rasterizer;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.gvt.renderer.ImageRenderer;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.batik.util.XMLResourceDescriptor;
import org.bitbucket.espinosa.svg.icon.rasterizer.ext.css.DoNotApplyExternalCss;
import org.bitbucket.espinosa.svg.icon.rasterizer.ext.css.ExternalCssApplicator;
import org.bitbucket.espinosa.svg.icon.rasterizer.fixers.DefaultSourceFixers;
import org.bitbucket.espinosa.svg.icon.rasterizer.fixers.LenientSaxSvgDocumentFactory;
import org.bitbucket.espinosa.svg.icon.rasterizer.fixers.SourceFixer;
import org.bitbucket.espinosa.svg.icon.rasterizer.log.Log;
import org.bitbucket.espinosa.svg.icon.rasterizer.log.LogEmptyImpl;
import org.w3c.dom.Document;
import org.w3c.dom.svg.SVGDocument;

/** 
 * Generic all purpose SVG to memory bitmap renderer; with main focus on
 * rendering UI icons. It can be used for both on-the-fly as-needed for running GUI
 * or for a bath operations converting whole sets of SVGs to PNGs.
 * 
 * Inspiration from:
 * /org.eclipse.images.renderer/src/main/java/org/eclipse/images/renderer/RenderMojo.java
 * from: https://git.eclipse.org/c/platform/eclipse.platform.images.git
 */
public class BatikRasterizer implements SvgRasterizer {

	/**
	 * Injected logger. It can be Maven logger if run as part of Maven batch
	 * (incompatible with anything else) or Eclipse logger, if run inside
	 * Eclipse, or standard log4j logger for anything else
	 */
	private Log log = new LogEmptyImpl();

	/**
	 * Apply external CSS to SVG rendering if an external style-sheet is
	 * defined. Full implementation is part of plug-in extending this plug-in.
	 * Without this, {@link DoNotApplyExternalCss} implementation is used.
	 * <p>
	 * Implementation can decide if there is one style-sheet per renderer or if
	 * per image style is also supported.
	 */
	private ExternalCssApplicator explicitCssApplicator = new DoNotApplyExternalCss();

	/**
	 * Provide flexible way how to define output size. It can be a "fixed" one,
	 * that is dimensions are defined externally, or "native" taking the size from
	 * SVG itself. For UI icon rendering it is always the fixed one.
	 */
	private final OutputSizeDefinition outputSizeDefinition;

	/**
	 * Provide flexible way how to determine color and alpha raster
	 * configuration for the "canvas" {@link BufferedImage} to which Batik
	 * renders the SVG image.
	 */
	private BufferedImageFactory bufferedImageFactory;

	/**
	 * Provide configuration of renderer in form of rendering "hints",
	 * influencing things like output quality.
	 */
	private final RendererHintConfiguration rendererHintConfiguration;

	/**
	 * Provide flexible way, if requested, to apply post load structural fixes
	 * to SVG DOM to prevent known Batik issues.
	 */
	private final SourceFixer sourceFixer;

	/**
	 * Convenience constructor with some default settings
	 */
	public BatikRasterizer() {
		this(new NativeOutputSize()); 
	}

	/**
	 * Convenience constructor with some default settings
	 */
	public BatikRasterizer(OutputSizeDefinition outputSizeDefinition) {
		this(outputSizeDefinition, 
				new HighQualityHintConfiguration(), 
				new DefaultSourceFixers()); 
	}

	/**
	 * Main constructor
	 */
	public BatikRasterizer(
			OutputSizeDefinition outputSizeDefinition,
			RendererHintConfiguration rendererHintConfiguration,
			SourceFixer sourceFixer) {
		this.outputSizeDefinition = outputSizeDefinition;
		this.bufferedImageFactory = new DefaultBufferedImageFactory();
		this.rendererHintConfiguration = rendererHintConfiguration;
		this.sourceFixer = sourceFixer;
	}

	/**
	 * Generates raster images from the input SVG vector image.
	 *
	 * @param icon
	 */
	public BufferedImage rasterize(SvgSource icon) throws RasterizeSvgException {
		SVGDocument svgDocument = loadSvgDocument(icon);
		sourceFixer.fixDocument(svgDocument);

		// for fixed size definitions following command doesn't do anything (and should be stripped off by JVM)
		OutputSize outputSize = outputSizeDefinition.extract(svgDocument, icon);

		// Render to SVG
		log.info(Thread.currentThread().getName() + " " + " Rasterizing: " + icon.getNameBase() + ".png at "
				+ outputSize.getWidth() + "x" + outputSize.getHeight());
		TranscoderInput svgInput = new TranscoderInput(svgDocument);
		return renderIconInternal(icon, outputSize.getWidth(), outputSize.getHeight(), svgInput);
	}

	/**
	 * Use batik to rasterize the input SVG into a raster image at the specified
	 * image dimensions.
	 *
	 * @param icon
	 * @param width
	 *            the width to render the icons at
	 * @param height
	 *            the height to render the icon at
	 * @param transcoderInput
	 *            the SVG transcoder input
	 * 
	 * @return true if the icon was rendered successfully, false otherwise
	 */
	protected BufferedImage renderIconInternal(SvgSource icon, int width, int height, TranscoderInput transcoderInput) throws RasterizeSvgException {
		BufferedImageTranscoder transcoder = new BufferedImageTranscoder(
				bufferedImageFactory, rendererHintConfiguration);
		// TODO: consider reusing of transcoder between calls

		// possibly apply external style-sheet  
		explicitCssApplicator.apply(icon, transcoderInput.getDocument(), transcoder);

		transcoder.addTranscodingHint(ImageTranscoder.KEY_WIDTH, new Float(width));
		transcoder.addTranscodingHint(ImageTranscoder.KEY_HEIGHT, new Float(height));

		// default error handler throws exception
		// it can be replaced with logging if following line is uncommented
		// transcoder.setErrorHandler(new LoggingTranscoderErrorHandler(log, icon));

		try {
			transcoder.transcode(transcoderInput, null);
		} catch (TranscoderException e) {
			throw new RasterizeSvgException("Transcoding failed", icon, e);
		}

		return transcoder.getRenderedImage();
	}

	/**
	 * Load and parse SVG stream to a DOM graph.
	 * 
	 * @param svgFileName
	 *            optional, typically document URI
	 * @param svgInputStream
	 *            SVG content as stream
	 * @return Batik compatible DOM {@link Document}
	 */
	protected SVGDocument loadSvgDocument(SvgSource icon) throws RasterizeSvgException {
		String parserName = XMLResourceDescriptor.getXMLParserClassName();
		SAXSVGDocumentFactory parser = new LenientSaxSvgDocumentFactory(parserName);
		SVGDocument svgDocument;
		try {
			InputStream svgInputStream = icon.getStream();
			svgDocument = (SVGDocument) parser.createDocument(icon.getResourceURI().toString(), svgInputStream);
		} catch (IOException e) {
			throw new RasterizeSvgException("Error parsing SVG icon document", icon, e);
		}
		return svgDocument;
	}

	public static class BufferedImageTranscoder extends ImageTranscoder {
		private BufferedImage renderedImage;
		private final BufferedImageFactory bufferedImageFactory;
		private final ImageRendererFactory imageRendererFactory;

		public BufferedImageTranscoder(
				BufferedImageFactory bufferedImageFactory,
				ImageRendererFactory imageRendererFactory) {
			this.bufferedImageFactory = bufferedImageFactory;
			this.imageRendererFactory = imageRendererFactory;
		}

		@Override
		protected ImageRenderer createRenderer() {
			return imageRendererFactory.createRenderer();
		}

		@Override
		public BufferedImage createImage(int w, int h) {
			return bufferedImageFactory.createImage(w, h);
		}

		@Override
		public void writeImage(BufferedImage image, TranscoderOutput out)
				throws TranscoderException {
			renderedImage = image;
		}

		public BufferedImage getRenderedImage() {
			return renderedImage;
		}
	}

	public void setBufferedImageFactory(BufferedImageFactory bufferedImageFactory) {
		this.bufferedImageFactory = bufferedImageFactory;
	}

	public static class DefaultBufferedImageFactory implements BufferedImageFactory {
		@Override
		public BufferedImage createImage(int w, int h) {
			return new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		}
	}
}
