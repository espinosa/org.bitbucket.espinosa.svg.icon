package org.bitbucket.espinosa.svg.icon.rasterizer;

public class OutputSize {
	private final int width;
	private final int height;
	
	public OutputSize(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
}
