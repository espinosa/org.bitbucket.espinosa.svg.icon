package org.bitbucket.espinosa.svg.icon.rasterizer;

import java.awt.image.BufferedImage;

public interface BufferedImageFactory {
	public BufferedImage createImage(int w, int h);
}
