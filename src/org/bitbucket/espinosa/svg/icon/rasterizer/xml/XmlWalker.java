package org.bitbucket.espinosa.svg.icon.rasterizer.xml;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Deep First XML node tree traversal (DFS style), iterative (non-recursive),
 * featuring visitBefore and visitAfter functionality.
 * 
 * TODO: add local unit tests
 * 
 * Note: this class is from my another project, it is extensively unit tested
 * there
 * 
 * @author Espinosa
 */
public abstract class XmlWalker {

	/**
	 * Define action performed on entering an element; called for every Element
	 * in the model.
	 */
	protected abstract void visitBefore(Element element);

	/**
	 * Define action performed on leaving an element; called for every Element
	 * in the model.
	 */
	protected abstract void visitAfter(Element element);

	/**
	 * Traverse given DOM document, calling {@link #visitBefore(Element)} and
	 * {@link #visitAfter(Element)} for every {@link Element} in the model.
	 */
	public void walk(Document document) {
		Deque<StackItem> stack = new LinkedList<>(); 
		stack.add(new StackItem(document.getDocumentElement()));
		visitBefore(document.getDocumentElement());
		while (stack.size() > 0) {
			StackItem s = stack.peek();
			if (s.hasNext()) {
				Node n = s.next();
				if (n instanceof Element) {
					Element element = (Element)n;
					visitBefore(element);
					stack.push(new StackItem(element));
				}
			} else {
				// no more children on this level
				// moving up
				visitAfter(s.parent);
				stack.pop();
			}
		}
	}

	/**
	 * Helper class, facilitate the traversal
	 */
	class StackItem implements Iterator<Node> {
		private final NodeList nodeList;
		private final Element parent;
		private int index = 0;

		public StackItem(Element element) {
			this.parent = element;
			this.nodeList = element.getChildNodes();
		}

		@Override
		public boolean hasNext() {
			return index < nodeList.getLength();
		}

		@Override
		public Node next() {
			if (hasNext()) {
				return nodeList.item(index++);
			} else {
				throw new NoSuchElementException();
			}  
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String toString() {
			String currentElementName = null;
			if (index < nodeList.getLength()) {
				currentElementName = nodeList.item(index).getNodeName();
			}
			return "StackItem ["
					+ "parent=" + parent + ", "
					+ "index=" + index + ", " 
					+ "currentElement=" + currentElementName
					+ "]";
		}
	}
}
