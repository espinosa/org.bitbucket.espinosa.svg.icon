package org.bitbucket.espinosa.svg.icon.rasterizer.fixers;

import java.util.Arrays;
import java.util.List;

import org.bitbucket.espinosa.svg.icon.rasterizer.xml.XmlWalker;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;

/**
 * Define set of requested SVG DOM source fixes to prevent known Batik issues.
 * 
 * Traversing, {@link XmlWalker} based, {@link SourceFixer} with set of given
 * {@link SourceFixerAction}.
 * 
 * @author Espinosa
 */
public class TraversingSourceFixer extends XmlWalker implements SourceFixer {
	
	/** registered fixers */
	private final List<SourceFixerAction> fixers;

	public TraversingSourceFixer(SourceFixerAction... fixers) {
		this.fixers = Arrays.asList(fixers);
	}

	@Override
	public void fixDocument(SVGDocument svgDocument) {
		if (!fixers.isEmpty()) {
			walk(svgDocument);
		}	
	}

	@Override
	protected void visitBefore(Element element) {
		for (SourceFixerAction fixer : fixers) {
			// apply fix, if applicable for the given element
			fixer.apply(element);
		}
	}

	@Override
	protected void visitAfter(Element element) {
		// do nothing
	}
}
