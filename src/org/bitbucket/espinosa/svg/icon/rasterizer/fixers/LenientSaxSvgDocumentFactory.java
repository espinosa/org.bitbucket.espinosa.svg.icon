package org.bitbucket.espinosa.svg.icon.rasterizer.fixers;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.anim.dom.SVG12DOMImplementation;
import org.apache.batik.anim.dom.SVGDOMImplementation;
import org.w3c.dom.DOMImplementation;

/**
 * Modify original Batik  {@link SAXSVGDocumentFactory} to accept also {@code version="1"}
 * which I found common in Papirus Icon Theme SVGs.
 */
public class LenientSaxSvgDocumentFactory extends SAXSVGDocumentFactory {
	public LenientSaxSvgDocumentFactory(String parser) {
		super(parser);
	}
	
	@Override
    public DOMImplementation getDOMImplementation(String ver) {
	    // code is mostly rip-off from original Apache Batik 1.9 code
        // only the condition was extended to accept also "1" string
        if (ver == null || ver.length() == 0
                || ver.equals("1.0") || ver.equals("1.1") || ver.equals("1")) {
            return SVGDOMImplementation.getDOMImplementation();
        } else if (ver.equals("1.2")) {
            return SVG12DOMImplementation.getDOMImplementation();
        }
        // FIXME: better exception
        throw new RuntimeException("Unsupported SVG version '" + ver + "'");
    }
}