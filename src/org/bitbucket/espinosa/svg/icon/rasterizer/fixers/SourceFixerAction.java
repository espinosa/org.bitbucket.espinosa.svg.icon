package org.bitbucket.espinosa.svg.icon.rasterizer.fixers;

import org.w3c.dom.Element;

/**
 * Apply, if applicable for the given element, a particular fix. 
 */
public interface SourceFixerAction {
	void apply(Element element);
}
