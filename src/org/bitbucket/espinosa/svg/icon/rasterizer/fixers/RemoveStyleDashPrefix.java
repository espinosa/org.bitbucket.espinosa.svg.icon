package org.bitbucket.espinosa.svg.icon.rasterizer.fixers;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

/**
 * Removes broken inkscape prefix from documents, preventing broken
 * rendering with Batik, see:
 * <br>
 * https://issues.apache.org/jira/browse/BATIK-1112<br>
 * https://bugs.eclipse.org/bugs/show_bug.cgi?id=493994<br>
 */
public class RemoveStyleDashPrefix implements SourceFixerAction {

	@Override
	public void apply(Element element) {
		Attr attr = element.getAttributeNodeNS(null, "style");
		if (attr != null) {
			String style = attr.getValue();
			String replaceAll = style.replaceAll("-inkscape", "inkscape");
			element.setAttributeNS(null, "style", replaceAll);
		}
	}	
}
