package org.bitbucket.espinosa.svg.icon.rasterizer.fixers;

import org.w3c.dom.svg.SVGDocument;

/**
 * Provide flexible way, if requested, to apply post load structural fixes
 * to SVG DOM to prevent known Batik issues.
 * 
 * @author Espinosa
 */
public interface SourceFixer {
	void fixDocument(SVGDocument svgDocument);
}
