package org.bitbucket.espinosa.svg.icon.rasterizer.fixers;

/**
 * Define list of default (recommended) SVG source fixers.
 * 
 * @author Espinosa
 */
public class DefaultSourceFixers extends TraversingSourceFixer {
	public DefaultSourceFixers() {
		super(
				new RemoveStyleDashPrefix(),
				new DefaultMissingOffsetInGradientStyle()
				);
	}
}
