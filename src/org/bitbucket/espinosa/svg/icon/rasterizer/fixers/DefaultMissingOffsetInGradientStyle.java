package org.bitbucket.espinosa.svg.icon.rasterizer.fixers;

import org.w3c.dom.Element;

/**
 * See https://issues.apache.org/jira/browse/BATIK-1195
 * 
 * @author Espinosa
 */
public class DefaultMissingOffsetInGradientStyle implements SourceFixerAction {

	@Override
	public void apply(Element element) {
		if ( element.getNodeName().equals("stop") &&
				element.getParentNode() != null && 
				element.getParentNode().getNodeName().equals("linearGradient") &&
				element.getAttributeNodeNS(null, "offset") == null) {
			element.setAttribute("offset", "0");
		}
	}
}
