package org.bitbucket.espinosa.svg.icon.rasterizer;

import java.io.InputStream;
import java.net.URI;

public class SvgSource {

	/** The name of the icon minus extension */
	private final String nameBase;

	//	/** The input path of the source svg files. */
	//	// Optional
	//	// TODO: URI would be better, I'm not sure how Path would cope with inside JAR svg files
	//	private Path path;

	//	/** The sizes this icon should be rendered at */
	//	// FIXME: remove 'sizes'
	//	private int[] sizes;

	private final InputStream stream;

	private final URI resourceURI;

	public SvgSource(String nameBase, InputStream stream, URI resourceURI) {
		this.nameBase = nameBase;
		this.stream = stream;
		this.resourceURI = resourceURI;
	}

	//	/**
	//	 * 
	//	 * @param nameBase
	//	 * @param inputPath
	//	 * @param sizes
	//	 */
	//	public SvgSource(InputStream stream, String nameBase, Path inputPath, int[] sizes) {
	//		this.stream = stream;
	//		this.nameBase = nameBase;
	//		this.sizes = sizes;
	//		this.path = inputPath;
	//	}

	public InputStream getStream() {
		return stream;
	}

	public String getNameBase() {
		return nameBase;
	}

	public URI getResourceURI() {
		return resourceURI;
	}

	//	public Path getPath() {
	//		return path;
	//	}
	//
	//	public int[] getSizes() {
	//		return sizes;
	//	}
	
	
}