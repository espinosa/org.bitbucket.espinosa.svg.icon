package org.bitbucket.espinosa.svg.icon.rasterizer.ext.css;

import org.apache.batik.transcoder.image.ImageTranscoder;
import org.bitbucket.espinosa.svg.icon.rasterizer.SvgSource;
import org.w3c.dom.Document;

public interface ExternalCssApplicator {
	void apply(SvgSource icon, Document svgDocument, ImageTranscoder transcoder);
}
