package org.bitbucket.espinosa.svg.icon.rasterizer.ext.css;

import java.io.File;
import java.nio.file.Paths;

import org.apache.batik.transcoder.image.ImageTranscoder;
import org.bitbucket.espinosa.svg.icon.rasterizer.SvgSource;
import org.bitbucket.espinosa.svg.icon.rasterizer.log.Log;
import org.bitbucket.espinosa.svg.icon.rasterizer.log.LogEmptyImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ExternalCssApplicatorImpl implements ExternalCssApplicator {

	private final String stylesheetName;
	
	public ExternalCssApplicatorImpl(String stylesheetName) {
		this.stylesheetName = stylesheetName;
	}

	/**
	 * Injected logger. It can be Maven logger if run as part of Maven batch
	 * (incompatible with anything else) or Eclipse logger, if run inside
	 * Eclipse, or standard log4j logger for anything else
	 */
	private Log log = new LogEmptyImpl();

	@Override
	public void apply(SvgSource icon, Document svgDocument, ImageTranscoder transcoder) {
		if (stylesheetName != null) {
			// FIXME: this will not work in JARred themes/Bundled themes
			String cssRoot = Paths.get(icon.getResourceURI()).toAbsolutePath().toString().replace("eclipse-svg", "eclipse-css");
			cssRoot = cssRoot.replace("/icons/", "/styles/" + stylesheetName + "/");
			cssRoot = cssRoot.replace(".svg", ".scss");
			File cssPath = new File(cssRoot);

			File preprocessedCss = generateCSS(icon.getNameBase(), cssPath.getAbsolutePath());

			if (!preprocessedCss.exists()) {
				log.error("Could not resolve supplied stylesheet: " + preprocessedCss.getAbsolutePath()
				+ ", using defaults.");
			} else {
				removeInlineStyle(svgDocument.getDocumentElement());

				transcoder.addTranscodingHint(ImageTranscoder.KEY_USER_STYLESHEET_URI,
						preprocessedCss.toURI().toString());
			}
		}
	}

	/**
	 * Strips inline CSS styles from the supplied node and all descendants.
	 * 
	 * @param node usually whole document
	 */
	void removeInlineStyle(Node node) {
		NodeList nodes = node.getChildNodes();
		int len = nodes.getLength();
		for (int i = 0; i < len; i++) {
			Node item = nodes.item(i);

			if (item instanceof Element) {
				Element elem = (Element) item;
				elem.setAttributeNS(null, "style", "");
			}

			removeInlineStyle(item);
		}
	}

	/**
	 * Uses SASS to generate a CSS style sheet for the given style name.
	 */
	private File generateCSS(String styleName, String inputStylesheet) {
		// FIXME: copy implementation from original
		throw new RuntimeException("Not yet implemented");
	}
}
