package org.bitbucket.espinosa.svg.icon.rasterizer.ext.css;

import org.apache.batik.transcoder.image.ImageTranscoder;
import org.bitbucket.espinosa.svg.icon.rasterizer.SvgSource;
import org.w3c.dom.Document;

public class DoNotApplyExternalCss implements ExternalCssApplicator {
	
	@Override
	public void apply(SvgSource icon, Document svgDocument, ImageTranscoder transcoder) {
		// do nothing
	}
}
