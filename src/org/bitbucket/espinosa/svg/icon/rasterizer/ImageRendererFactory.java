package org.bitbucket.espinosa.svg.icon.rasterizer;

import org.apache.batik.gvt.renderer.ImageRenderer;

interface ImageRendererFactory {
	public ImageRenderer createRenderer();
}
