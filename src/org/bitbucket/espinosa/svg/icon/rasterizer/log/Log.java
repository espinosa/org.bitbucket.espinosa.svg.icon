package org.bitbucket.espinosa.svg.icon.rasterizer.log;

public interface Log {
	void info(String message);
	void error(String message);
	void error(String message, Throwable e);
}
