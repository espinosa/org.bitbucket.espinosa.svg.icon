package org.bitbucket.espinosa.svg.icon.rasterizer.log;

import org.apache.batik.transcoder.ErrorHandler;
import org.apache.batik.transcoder.TranscoderException;
import org.bitbucket.espinosa.svg.icon.rasterizer.SvgSource;

public class LoggingTranscoderErrorHandler implements ErrorHandler {
	private final Log log;
	private final SvgSource icon;
	
	public LoggingTranscoderErrorHandler(Log log, SvgSource icon) {
		this.log = log;
		this.icon = icon;
	}

	@Override
	public void warning(TranscoderException arg0) throws TranscoderException {
		log.error("Icon: " + icon + " - WARN: " + arg0.getMessage());
	}

	@Override
	public void fatalError(TranscoderException arg0) throws TranscoderException {
		log.error("Icon: " + icon + " - FATAL: " + arg0.getMessage());
	}

	@Override
	public void error(TranscoderException arg0) throws TranscoderException {
		log.error("Icon: " + icon + " - ERROR: " + arg0.getMessage());
	}
}
