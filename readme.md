SVG Icon Renderer for Eclipse, Eclipse E4, Eclipse RCP and SWT applications
===========================================================================

Main Features:

 * Eclipse PDE project
 * Based on Apache Batik 1.9.0 (most recent version)
 * intended for both on-the-fly SVG rendering for GUI applications and batch oriented rendering (like pre-rendering whole theme set to png).
 * intended primarily to render UI icons
 * flexible extendible design

TODO:
 
  * separate Eclipse independent parts into a separate project (the rasterizer itself, ie most of this project)
  * move out test application and sample icons to a separate project 
  * create a project demonstrating its usability in JavaFX or Swing applications
  * create a project demonstrating its usability in Maven and/or Ant batch builds (pre-rendered icons to PNG)
  * add support for external CSS styling (as an extension to this bundle)
  * add support for post-rendering filters; mainly intended to create "grayed" versions of icons (as an extension to this bundle)
  * add some sort of persistence for faster repeated application starts

## Code sample

SWT application:

	import org.bitbucket.espinosa.svg.icon.cache.CachedImageLoader;
	import org.bitbucket.espinosa.svg.icon.cache.CachedImageLoader.PropagatingErrorHandler;
	import org.bitbucket.espinosa.svg.icon.rasterizer.BatikRasterizer;
	import org.bitbucket.espinosa.svg.icon.rasterizer.FixedOutputSizeDefinition;
	import org.bitbucket.espinosa.svg.icon.rasterizer.HighQualityHintConfiguration;
	import org.bitbucket.espinosa.svg.icon.resmap.SimpleResourceIcons;
	import org.bitbucket.espinosa.svg.icon.swt.BufferedImageToSwtImageConverter1;
	import org.bitbucket.espinosa.svg.icon.test.icons.SampleTheme;
	import org.bitbucket.espinosa.svg.icon.test.icons5.SampleTheme5;
	
	public class ShowSelectedIcons {
	
		SampleTheme theme = new SampleTheme5();
	
		CachedImageLoader<Image> iml = new CachedImageLoader<>(
			new SimpleResourceIcons(theme.getClass()),     // pluggable icon theme resolver; mapping simple icon names to absolute FS paths or URIs
			new BatikRasterizer(                           // pluggable SVG renderer
					new FixedOutputSizeDefinition(16, 16), // pluggable output size definition (there is also NativeOutputDefinition class)
					new HighQualityHintConfiguration(),    // pluggable rendering hints for Batik
					new DefaultSourceFixers()),            // pluggable set of "fixers", to fix source SVG DOM from known Batik issues
			new BufferedImageToSwtImageConverter1(),       // pluggable final transformation
			new PropagatingErrorHandler<Image>()           // pluggable error handler
			);
		
		// ...
		Image swtImage = iml.getImage("folder.svg");
		cell.setImage(swtImage); // apply icon to table cell, button, label, .. 
	}

`CachedImageLoader` is thread safe, backed by `ConcurrentHashMap`.

## Screenshot 1
Screenshots of a simple showcase SWT application with Eclipse icon theme in 16x16 pixels:  
![Screenshots of a simple showcase SWT application with Eclipse theme](screenshots/2017-07-24%20SVG%20icons%20showcase%20app%20with%20Eclipse%20theme%20rendered%20as%2016x16%20cut.png)  
Icons source: git://git.eclipse.org/gitroot/platform/eclipse.platform.images.git  

## Screenshot 1b
Same Eclipse icon theme but rendered to 48x48 pixels:  
![Screenshots of a simple showcase SWT application with Eclipse theme 48x48](screenshots/2017-07-26%20SVG%20icons%20showcase%20advaced%20app%20with%20Eclipse%20theme%20rendered%20as%2048x48.png)  

## Screenshot 2
Screenshots of the same simple showcase SWT application this time with Papirus icon theme:  
![Screenshots of a simple showcase SWT application with Papirus theme](screenshots/2017-07-24%20SVG%20icons%20showcase%20app%20with%20Papirus%20theme%20rendered%20as%2016x16.png)  
Papirus is a SVG theme primarily for KDE/Gnome desktops.  
Icons source:  
https://github.com/PapirusDevelopmentTeam/papirus-icon-theme  
https://github.com/PapirusDevelopmentTeam/papirus-icon-theme/blob/master/Papirus/16x16/mimetypes/x-office-document.svg

## Screenshot 2b
Same Papirus icon theme but rendered to 48x48 pixels:  
![Screenshots of a simple showcase SWT application with Papirus theme 48x48](screenshots/2017-07-26%20SVG%20icons%20showcase%20advaced%20app%20with%20Papirus%20theme%20rendered%20as%2048x48.png)  

## Screenshot 3
Screenshots of the same simple showcase SWT application this time with mixed icon theme:  
![Screenshots of a simple showcase SWT application with mixed theme](screenshots/2017-07-24%20SVG%20icons%20showcase%20app%20with%20mixed%20theme%20rendered%20as%2016x16.png)    
Icons source: various places on Internet, mostly Wikipedia and Eclipse GIT.

## Screenshot 3b
Same mixed icon theme but rendered to 48x48 pixels:  
![Screenshots of a simple showcase SWT application with mixed theme 32x32](screenshots/2017-07-26%20SVG%20icons%20showcase%20advaced%20app%20with%20mixed%20folders%20theme%20rendered%20as%2032x32.png)  
